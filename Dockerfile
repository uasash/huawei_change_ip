FROM python:3
EXPOSE 8000
RUN pip install uvicorn requests xmltodict fastapi huawei_lte_api

WORKDIR /app

COPY main.py /app/

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]

