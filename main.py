import uvicorn
import requests
import xmltodict
import time
from fastapi import FastAPI
from huawei_lte_api.Client import Client
from huawei_lte_api.Connection import Connection
from huawei_lte_api.enums.client import ResponseEnum

def dataswitch(base_url, status):
    session = requests.Session()
    reqresponse = session.get(base_url + '/api/webserver/SesTokInfo')
    if reqresponse.status_code == 200:
           _dict = xmltodict.parse(reqresponse.text).get('response', None)
    headers = {'Content-Type': 'text/xml; charset=UTF-8','Cookie': _dict['SesInfo'],
            '__RequestVerificationToken': _dict['TokInfo']
                  }
    api_url = base_url + '/api/dialup/mobile-dataswitch'
    data=f'<?xml version="1.0" encoding="UTF-8"?><request><dataswitch>{status}</dataswitch></request>'
    result = session.post( api_url, data=data, headers=headers)
    return result.text

def reboot(base_url):
    session = requests.Session()
    reqresponse = session.get(base_url + '/api/webserver/SesTokInfo')
    if reqresponse.status_code == 200:
           _dict = xmltodict.parse(reqresponse.text).get('response', None)
    headers = {'Content-Type': 'text/xml; charset=UTF-8','Cookie': _dict['SesInfo'],
            '__RequestVerificationToken': _dict['TokInfo']
                  }
    api_url = base_url + '/api/device/control'
    data=f'<?xml version="1.0" encoding="UTF-8"?><request><Control>1</Control></request>'
    result = session.post( api_url, data=data, headers=headers)
    return result

def change_ip(base_url):
    with Connection(base_url) as connection:
      client = Client(connection)
      print('Disabling dialup...')
      if client.dial_up.set_mobile_dataswitch(0) == ResponseEnum.OK.value:
          disable_status = 'OK' 
      else:
          disable_status = 'Error'
      print('Enabling dialup...')
      if client.dial_up.set_mobile_dataswitch(1) == ResponseEnum.OK.value:
          enable_status = 'OK'
      else:
          enable_status = 'Error'
    time.sleep(2)
    ip = requests.get('http://ifconfig.me')
    result = {'enable_status': enable_status,
              'disable_status': disable_status,
              'ip':   ip.text
              }
    return(result)


app = FastAPI()

@app.get("/")
def read_root():
    return {"Error": "Please use /{ device id }/change_ip"}


@app.get('/{modem_id}/change_ip')
def read_item(modem_id):
    base_url = f'http://192.168.{ modem_id }.1'
    execute_change_ip = change_ip(base_url)
    responce_json = {
        "status": execute_change_ip['enable_status'],
        "code": "" ,
        "new_ip": execute_change_ip['ip'],
        "rt": "",
        "proxy_id": modem_id
        }
    return(responce_json)

@app.get('/{modem_id}/reboot')
def read_item(modem_id):
    base_url = f'http://192.168.{ modem_id }.1'
    reboot_cmd = reboot(base_url)
    responce_json = {
        "status": reboot_cmd.reason,
        "code": reboot_cmd.status_code ,
        "new_ip": "",
        "rt": "",
        "proxy_id": modem_id
        }
    return(responce_json)
    
if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
