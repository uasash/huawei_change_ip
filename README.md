# huawei_change_ip



## Установка зависимостей
```bash 
pip install uvicorn requests xmltodict fastapi
```
## Запуск скипта
```bash
puthon ./main.py
```
## пример использования 
```bash
curl http://localhost:8000/8/change_ip
```

## Запуск докера 
```bash
docker run -d -p 8000:8000  registry.gitlab.com/uasash/huawei_change_ip:latest
```